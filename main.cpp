#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <cstdio>

enum {
  MAT_RANGE = 10,
  RANDOM_SEED = 1,
  ITER_TO_FIND_MAX = 4,
  MAX_RANK = 150
};

double *_MAIN_ORIGINAL; // don't know how interpret matrix as a function
int _MAIN_ORIGINAL_SIZE;
// without global variable


void print_mat(double *mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%6.1f ", mat[i * cols + j]);
        }
        std::cout << "\n";
    }
    std::cout<<"\n\n";
}

double *mat_gen(int n, int rank) {
    double *mat_r = new double[n * rank];
    double *mat_l = new double[rank * n];
    double *mult = new double[n * n];
    for(int i = 0; i < n * rank; i++) {
        mat_l[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
        mat_r[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            mult[i * n + j] = 0;
            for (int k = 0; k < rank; k++) {
                //mult[i][j] = mat_l[i][k] * mat_r[k][j]
                mult[i * n + j] += mat_l[i * rank + k] * mat_r[k * n + j];
            }
        }
    }
    delete mat_r;
    delete mat_l;
    return mult;
}


void print_matmul(double *a, double *b, int n, int p) { // for debug only
    double *res = new double[n * n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            res[i * n + j] = 0;
            for (int k = 0; k < p; k++) {
                res[i * n + j] += a[i * p + k] * b[k * n + j];
            }
        }
    }
    print_mat(res, n, n);
    delete[] res;
}

void print_matmul_diff(double (*f)(int, int), double *a, double *b, int n, int p, int m) { // for debug only
    double *res = new double[n * m];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            res[i * m + j] = f(i, j);
            for (int k = 0; k < p; k++) {
                res[i * m + j] -= a[i * p + k] * b[k * m + j];
            }
        }
    }
    print_mat(res, n, m);
    delete[] res;
}

int argmax_i(double (*A)(int, int), int *i_indicies, int *j_indicies, double *U, double *V, int r, int n) {
    int j_one;
    for (j_one = 0; j_one < n && j_indicies[j_one] < 0; j_one++) { ; }
    std::cout<<j_one<<"j\n";
    double val_max = 0;
    int argmax = 0;
    for (int i = 0; i < n; i++) {
        if (i_indicies[i] < 0) {
            continue;
        }
        double el = A(i, j_one);
        for (int k = 0; k < r; k++) {
            el -= U[k * n + i] * V[k * n + j_one]; // uidx
        }
        if (abs(el) > abs(val_max)) {
            val_max = el;
            argmax = i;
        }
    }
    return argmax;
}

int argmax_j(double (*A)(int, int), int i, int *j_indicies, double *U, double *V, int r, int n, double *max_el) {
    double val_max = 0;
    int argmax = 0;
    for (int j = 0; j < n; j++) {
        if (j_indicies[j] < 0) {
            continue;
        }
        double el = A(i, j);
        for (int k = 0; k < r; k++) {
            el -= U[k * n + i] * V[k * n + j];
        }
        if (abs(el) > abs(val_max)) {
            val_max = el;
            argmax = j; 
        }
    }
    *max_el = val_max;
    return argmax;
}


double norm_squared(double *u, int size)
{
    double res = 0;
    for (int i = 0; i < size; i++) {
        res += u[i] * u[i];
    }
    return res;
}

double calc_norm(double old_norm, double *U, double *V, double *u, double *v, int n, int r)
{
    if (r == 0) {
        return 0;
    }
    double inner = 0;
    for (int i = 0; i < r; i++) {
        double utu_i = 0;
        double vvt_i = 0;
        for (int k = 0; k < n; k++) {
            utu_i += U[i * n + k] * u[k];
            vvt_i += V[i * n + k] * v[k];
        }
        inner += utu_i * vvt_i;
    }
    return old_norm + 2 * inner + norm_squared(u, n) * norm_squared(v, n);
}

void vstack(double **V, double *v, int n, int r) {
    for (int i = 0; i < n; i++) {
        (*V)[r * n + i] = v[i];
    }
}


// u is stored column-major
void run_cross(double (*A_func)(int, int), int n, double eps, double **U, double **V, int *r) {
    *U = new double[n * MAX_RANK];
    *V = new double[n * MAX_RANK];
    int *i_indicies = new int[n];
    int *j_indicies = new int[n];
    for (int k = 0; k < n; k++) {
        i_indicies[k] = j_indicies[k] = k;
    }
    *r = 0;
    double uv_norm = 0;
    double *u = new double[n];
    double *v = new double[n];
    
    while (*r < n) {
        double b_ij;
        int i_idx = argmax_i(A_func, i_indicies, j_indicies, *U, *V, *r, n);
        int j_idx = argmax_j(A_func, i_idx, j_indicies, *U, *V, *r, n, &b_ij);
        printf("%d %d %lf\n", i_idx, j_idx, b_ij);
        uv_norm = calc_norm(uv_norm, *U, *V, u, v, n, *r);
        if (eps * uv_norm >= abs(b_ij) * (n - *r)) {
            break;
        }
        for (int t = 0; t < n; t++) {
            u[t] = A_func(t, j_idx) / sqrt(fabs(b_ij));
            for (int k = 0; k < *r; k++) {
                u[t] -= (*U)[k * n + t] * (*V)[k * n + j_idx] / sqrt(fabs(b_ij));
            }
        }
        for (int t = 0; t < n; t++) {
            v[t] = A_func(i_idx, t) * sqrt(fabs(b_ij)) / b_ij;
            for (int k = 0; k < *r; k++) {
                v[t] -= (*U)[k * n + i_idx] * (*V)[k * n + t] * sqrt(fabs(b_ij)) / b_ij;
            }
        }
        vstack(U, u, n, *r);
        vstack(V, v, n, *r);
        i_indicies[i_idx] = -1;
        j_indicies[j_idx] = -1;
        *r += 1;
   }
}

double frac_ij(int i, int j) {
    return 1. / (i + j + 1.);
}

double get_orignal_val(int i, int j) {
    return _MAIN_ORIGINAL[i * _MAIN_ORIGINAL_SIZE + j];
}

int main(int argc, char *argv[]) {
    // srand(RANDOM_SEED);

    if (argc != 3) {
        std::cout << "Provide n, rank\n";
        return 1;
    }
 
    int n, rank;
    n = std::stoi(argv[1]);
    rank = std::stoi(argv[2]);

    double *U, *V;
    double eps = 1e-16;
    if (rank == 0) {
        run_cross(frac_ij, n, eps, &U, &V, &rank);
    } else {
        
           _MAIN_ORIGINAL_SIZE = n;
           _MAIN_ORIGINAL = mat_gen(n, rank);
           run_cross(get_orignal_val, n, eps, &U, &V, &rank);
           delete[] _MAIN_ORIGINAL;
    }
    printf("rank = %d\n", rank); 
    
    delete[] U;
    delete[] V;
    return 0;
}
