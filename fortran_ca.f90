module cross 
    implicit none
    private
    public print_mat, mat_gen, find_max, run_cross
    integer :: ITER_TO_FIND_MAX = 1 
    contains

    subroutine print_mat(A)
        implicit none
        real, intent(in) :: A(:,:)
        integer :: i

        do i = 1, size(A, 1) 
            print *, A(i, :)
        end do
        print *, ''
    end subroutine print_mat

    subroutine mat_gen(n, rank, mat_r) 
        implicit none
        integer, intent(in) :: n, rank
        real, intent(inout), allocatable :: mat_r(:,:)
        
        allocate(mat_r(n, rank))
        call RANDOM_NUMBER(mat_r) ! caps to show it's external function
    end subroutine mat_gen

    subroutine find_max(col, row, column_after, b_max, mat)
        implicit none
        integer, intent(inout) :: col, row, column_after
        real, intent(inout) :: mat(:,:)
        real, intent(inout) :: b_max
        integer :: step

        do step = 1, ITER_TO_FIND_MAX
            row = maxloc(abs(mat(:, col)), 1)
            col = maxloc(abs(mat(row, :)), 1)
        end do
        b_max = mat(row, col)
        
        mat(row, col) = 0
        column_after = maxloc(abs(mat(row, :)), 1)
        mat(row, col) = b_max
    end subroutine find_max

    subroutine run_cross(mat, n, max_steps, aprox)
        implicit none
        real, intent(inout) :: mat(:,:), aprox(:,:)
        integer, intent(in) :: n
        integer, intent(inout) :: max_steps

        real, allocatable :: diff(:, :), crb(:, :)
        real :: diff_norm, b
        integer :: col = 0, row = 0, column_after, mat_norm, i, j, k

        allocate(diff(n, n), crb(n, n))
        mat_norm = norm2(mat)
        aprox = 0.0
        diff = mat

        do i = 1, max_steps
            call find_max(col, row, column_after, b, diff)
            crb = spread(diff(:, col), dim=2, ncopies=n) * spread(diff(row, :), dim=1, ncopies=n) / b
            diff = diff - crb
            aprox = aprox + crb 
            diff_norm = norm2(diff)
            col = column_after
            row = 0
            if (diff_norm / mat_norm .le. 1.0e-6) then 
                max_steps = i
                exit
            end if
        end do
    end subroutine run_cross

end module cross

program cross_f90
    use cross
    implicit none
    integer :: n, rank, approx_rank, i, arglen=4
    character(len=32) :: arg
    real, allocatable :: original(:,:), mat_r1(:, :), mat_r2(:, :), aprox(:,:)

    call getarg(1, arg)
    ! https://stackoverflow.com/questions/24071722/converting-a-string-to-an-integer-in-fortran-90 
    read(arg, *, iostat=arglen) n
    call getarg(2, arg)
    read(arg, *, iostat=arglen) rank 

    call mat_gen(n, rank, mat_r1)
    call mat_gen(rank, n, mat_r2)
    original = matmul(mat_r1, mat_r2) * 1000
    allocate(aprox(n, n))
    call run_cross(original, n, n, aprox) 
    print *, 'aprox rank is ', n 
end program cross_f90

