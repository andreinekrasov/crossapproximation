from .mymodule import *

def run_cross(n, rank, aprox_rank):
    original = mat_gen(n, rank)
    aprox = run_cross_approximation(original, n, aprox_rank)
    return original, aprox
