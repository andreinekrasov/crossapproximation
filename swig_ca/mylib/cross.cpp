#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <cstdio>
#include "cross.h"


void print_mat(double *mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%6.1f ", mat[i * cols + j]);
        }
        std::cout << "\n";
    }
}

double *mat_gen(int n, int rank) {
    double *mat_r = new double[n * rank];
    double *mat_l = new double[rank * n];
    double *mult = new double[n * n];
    for(int i = 0; i < n * rank; i++) {
        mat_l[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
        mat_r[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            mult[i * n + j] = 0;
            for (int k = 0; k < rank; k++) {
                //mult[i][j] = mat_l[i][k] * mat_r[k][j]
                mult[i * n + j] += mat_l[i * rank + k] * mat_r[k * n + j];
            }
        }
    }
    delete mat_r;
    delete mat_l;
    return mult;
}

void copy_matrix(double **mat_cp, double *mat, int rows, int cols) {
    for (int i = 0; i < rows * cols; i++) {
       (* mat_cp)[i] = mat[i];
    }
}

void find_max(int *col, int *row, int *column_after, double *b_max, double *mat, int n) {
    for (int step = 0; step < ITER_TO_FIND_MAX; step++) {
        *b_max = mat[0 * n + *col];
        // find row with maximum in set column
        for (int i = 0; i < n; i++) {
            if (abs(mat[i * n + *col]) >= abs(*b_max)) {
                *b_max = mat[i * n + *col];
                *row = i;
            }
        }
        // find col with maximum in found row
        for (int j = 0; j < n; j++) {
            if (abs(mat[*row * n + j]) >= abs(*b_max)) {
                *b_max = mat[*row * n + j];
                *col = j;
            }
        }
    }
    int r_max = mat[*row * n + 0];
    *column_after = 0;
    for (int i = 0; i < n; i++)
    {
        if (i == *row)
        {
            continue;
        }
        if (abs(r_max) <  (mat[*row * n + i]))
        {
            r_max = mat[*row * n + i];
            *column_after = i;
        }
    }
}

double norm_fro(double *mat, int n) {
    double mat_norm = 0;
    for (int i = 0; i < n * n; i++) {
        mat_norm += mat[i] * mat[i];
    }
    return sqrt(mat_norm);
}

double *run_cross_approximation(double *mat, int n, int max_steps) {
    double *diff = new double[n*n];
    copy_matrix(&diff, mat, n, n);
    double *aprox = new double[n*n];
    for (int i = 0; i < n * n; i++) {
        aprox[i] = 0;
    }
    double diff_norm;
    int col = 0, row = 0, column_after;
    double b = mat[row * n + col];
    
    int mat_norm = norm_fro(mat, n);
    double *cp_diff = new double[n*n];
    for (int step = 1; step <= max_steps; step++) { 
        find_max(&col, &row, &column_after, &b, diff, n);
        if (b == 0) {
            std::cout<<norm_fro(diff, n)/mat_norm<<"\n"<<DBL_EPSILON<<"\n";
            return aprox;
        }
        copy_matrix(&cp_diff, diff, n, n); 
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                diff[i * n + j] -= cp_diff[n * i + col] * cp_diff[row * n + j] / b;
                aprox[i * n + j] += cp_diff[n * i +  col] * cp_diff[row * n + j] / b; 
            }
        }
        diff_norm = norm_fro(diff, n);
        col = column_after;
        row = 0;
        if (diff_norm / mat_norm < 100 * DBL_EPSILON) { // 
            std::cout << "converged with rank=" << step << "\n";
            break;
        }
    }
    delete cp_diff;
    delete diff;
    return aprox;
}

int main(int argc, char *argv[]) {
    srand(RANDOM_SEED);

    if (argc != 4) {
        std::cout << "Provide n, rank and aproximation rank in command line arguments\n";
        return 1;
    }
 
    int n, rank, aprox_rank;
    n = std::stoi(argv[1]);
    rank = std::stoi(argv[2]);
    aprox_rank = std::stoi(argv[3]);
    double *original = mat_gen(n, rank);
    
    double *aprox = run_cross_approximation(original, n, aprox_rank);
    if (n <= 10){
        std::cout<<"orginal\n";
        print_mat(original, n, n);
        std::cout<<"aproximation\n";
        print_mat(aprox, n, n);
    }
    delete original;
    delete aprox;
    return 0;
}
