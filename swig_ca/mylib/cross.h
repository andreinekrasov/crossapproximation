#ifndef __CROSS_H__
#define __CROSS_H__

enum {
  MAT_RANGE = 10,
  RANDOM_SEED = 1,
  ITER_TO_FIND_MAX = 4
};

void print_mat(double *mat, int rows, int cols); 

double *mat_gen(int n, int rank);

void copy_matrix(double **mat_cp, double *mat, int rows, int cols);

void find_max(int *col, int *row, int *column_after, double *b_max, double *mat, int n);

double norm_fro(double *mat, int n);

double *run_cross_approximation(double *mat, int n, int max_steps);
#endif


