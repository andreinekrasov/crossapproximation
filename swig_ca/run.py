import mylib
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int)
    parser.add_argument("rank", type=int)
    parser.add_argument("aprox_rank", type=int)

    args = parser.parse_args()

    orig, aprox = mylib.run_cross(args.n, args.rank, args.aprox_rank)

