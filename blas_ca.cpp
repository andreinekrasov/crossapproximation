#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <cstdio>
#include <cblas.h>

enum {
  MAT_RANGE = 10,
  RANDOM_SEED = 1,
  ITER_TO_FIND_MAX = 4
};

void print_mat(double *mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%6.1f ", mat[i * cols + j]);
        }
        std::cout << "\n";
    }
    std::cout<<"\n";
}

double *mat_gen(int n, int rank) {
    double *mat_r = new double[n * rank];
    double *mat_l = new double[rank * n];
    double *mult = new double[n * n];
    for(int i = 0; i < n * rank; i++) {
        mat_l[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
        mat_r[i] = MAT_RANGE / 2. + rand() / (RAND_MAX / (double) MAT_RANGE);
    }
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, rank, 1.0, mat_l, rank, mat_r, n, 0, mult, n);
    delete mat_l;
    delete mat_r;
    return mult;
}

void find_max(int *col, int *row, int *column_after, double *b_max, double *mat, double *vec, int n) {
    for (int step = 0; step < ITER_TO_FIND_MAX; step++) {
        cblas_dcopy(n, mat + *col, n, vec, 1);
        *row = cblas_idamax(n, vec, 1); 
        cblas_dcopy(n, mat + n * *row, 1, vec, 1); 
        *col = cblas_idamax(n, vec, 1);
    }
    *b_max = mat[*row * n + *col];
    cblas_dcopy(n, mat + n * *row, 1, vec, 1); 
    vec[*col] = 0;
    *column_after = cblas_idamax(n, mat + n * *row, 1);
}


double *run_cross_approximation(double *mat, int n, int max_steps) {
    double *diff = new double[n*n];
    cblas_dcopy(n * n, mat, 1, diff, 1);
    double *aprox = new double[n*n];
    for (int i = 0; i < n * n; i++) {
        aprox[i] = 0;
    }
    double diff_norm;
    int col = 0, row = 0, column_after;
    double b = mat[row * n + col];
    
    int mat_norm = cblas_dnrm2(n * n, mat, 1);
    double *col_vec = new double[n];
    double *row_vec = new double[n];
    for (int step = 1; step <= max_steps; step++) { 
        find_max(&col, &row, &column_after, &b, diff, col_vec, n);
        if (b == 0) {
            diff_norm = cblas_dnrm2(n * n, diff, 1);
            std::cout<<diff_norm / mat_norm<<"\n"<<DBL_EPSILON<<"\n";
            return aprox;
        }


        cblas_dcopy(n, diff + n * row, 1, row_vec, 1); // should be avoided, can be achieved via ?ger
        cblas_dcopy(n, diff + col, n, col_vec, 1);

        cblas_dger(CblasRowMajor, n, n, -1/b, col_vec, 1, row_vec, 1, diff, n);
        cblas_dger(CblasRowMajor, n, n, +1/b, col_vec, 1, row_vec, 1, aprox, n);
        
        // cblas_dger(CblasRowMajor, n, n, -1/b, diff + col, n, diff + n * row, 1, diff, n); 
        // cblas_dger(CblasRowMajor, n, n, +1/b, diff + col, n, diff + n * row, 1, aprox, n); 

        diff_norm = cblas_dnrm2(n * n, diff, 1);
        col = column_after;
        row = 0;
        if (diff_norm / mat_norm < 100 * DBL_EPSILON) { // 
            std::cout << "converged with rank=" << step << "\n";
            break;
        }
    }
    delete col_vec;
    delete row_vec;
    delete diff;
    return aprox;
}

int main(int argc, char *argv[]) {
    srand(RANDOM_SEED);

    if (argc != 4) {
        std::cout << "Provide n, rank and aproximation rank in command line arguments\n";
        return 1;
    }
 
    int n, rank, aprox_rank;
    n = std::stoi(argv[1]);
    rank = std::stoi(argv[2]);
    aprox_rank = std::stoi(argv[3]);
    double *original = mat_gen(n, rank);
    
    double *aprox = run_cross_approximation(original, n, aprox_rank);
    if (n <= 10){
        std::cout<<"orginal\n";
        print_mat(original, n, n);
        std::cout<<"aproximation\n";
        print_mat(aprox, n, n);
    }


    delete original;
    delete aprox;
    return 0;
}
