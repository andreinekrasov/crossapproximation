#!/bin/bash
declare -a arr=("python3 python_ca.py" "python3 numpy_ca.py" "./fortran_ca.out")

## now loop through the above array
for command in "${arr[@]}"
do
    for n in 128 256 512 1024 2048
    do
        for rank in 16 64 128 
        do
            echo "$command $n $rank $n"
            time $command $n $rank $n 
        done
    done
    echo "done"
done

