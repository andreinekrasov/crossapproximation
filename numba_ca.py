from numba import njit
import numpy as np
import random
import argparse
import time


MAT_RANGE = 10
RANDOM_SEED = 1
ITER_TO_FIND_MAX = 4

@njit()
def mat_gen(n: int, rank: int):
     mat_l = np.zeros((n, rank))
     mat_r = np.zeros((rank, n))
     mult = np.zeros((n, n))
     for i in range(0, n):
         for j in range(0, rank):
                 mat_l[i][j] = random.uniform(-MAT_RANGE, MAT_RANGE)
                 mat_r[i][j] = random.uniform(-MAT_RANGE, MAT_RANGE)

     for i in range(0, n):
         for j in range(0, n):
             mult[i][j] = 0
             for k in range(0, rank):
                 mult[i][j] += mat_l[i][k] * mat_r[k][j]
     return mult;

def mat_gen(n: int, rank: int):
    mat_r = np.zeros((n, rank))
    mult = np.zeros((n, n))
    for i in range(0, n):
        for j in range(0, rank):
                mat_r[i][j] = random.uniform(-MAT_RANGE, MAT_RANGE)

    for i in range(0, n):
        for j in range(0, n):
            mult[i][j] = 0
            for k in range(0, rank):
                mult[i][j] += mat_r[i][k] * mat_r[j][k]
    return mult;

@njit()
def find_max(col: int, row: int, column_after: int, b_max: float, mat, n: int):
    for ste in range(0, ITER_TO_FIND_MAX):
        b_max = mat[0][col]
        for i in range(0, n):
            if abs(mat[i][col]) > abs(b_max):
                b_max = mat[i][col]
                row = i
        
        for j in range(0, n):
            if abs(mat[row][j]) >= abs(b_max):
                b_max = mat[row][j]
                col = j
    r_max = mat[row][0];
    column_after = 0;
    for i in range(0, n):
        if i == row:
            continue
        if abs(r_max) < abs(mat[row][i]):
            r_max = mat[row][i]
            column_after = i
    return col, row, column_after, b_max

@njit()
def norm_fro(mat, n: int):
    mat_norm = 0;
    for i in range(0, n):
        for j in range(0, n):
            mat_norm += mat[i][j] ** 2
    return mat_norm ** .5

@njit()
def run_cross_approximation(mat, n: int, max_steps: int, precision: float):
    diff = np.zeros((n, n))
    diff = mat.copy()
    aprox = np.zeros((n, n))
    for i in range(0, n):
        for j in range(0, n):
            aprox[i][j] = 0
    diff_norm = 0
    col = 0
    row = 0
    column_after = 0
    b = mat[row][col]
    
    mat_norm = norm_fro(mat, n)
    cp_diff = np.zeros((n, n))
    for step in range(1, max_steps+1):
        col, row, column_after, b = find_max(col, row, column_after, b, diff, n)
        if (b == 0):
            print("b==0")
            return aprox;
        cp_diff = diff.copy()
        for i in range(0, n):
            for j in range(0, n):
                diff[i][j] -= cp_diff[i][col] * cp_diff[row][j] / b
                aprox[i][j] += cp_diff[i][col] * cp_diff[row][j] / b
        diff_norm = norm_fro(diff, n);
        col = column_after;
        row = 0;
        if (diff_norm / mat_norm < precision): 
            print(f"converged with rank={step}")
            break
    return aprox;


if __name__ == '__main__':
    random.seed(RANDOM_SEED)

    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int)
    parser.add_argument("rank", type=int)
    parser.add_argument("aprox_rank", type=int)

    args = parser.parse_args()

    original = mat_gen(args.n, args.rank)
    
    start = time.time()
    aprox = run_cross_approximation(original, args.n, 2, 100 * np.finfo(float).eps)
    print(f"Elapsed (with compilation) = {time.time() - start}")

    
    start = time.time()
    aprox = run_cross_approximation(original, args.n, args.aprox_rank, 100 * np.finfo(float).eps)
    print(f"Elapsed (no compilation) = {time.time() - start}")


    if args.n <= 10:
        print(original)
        print(aprox)

