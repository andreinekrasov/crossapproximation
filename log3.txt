python3 numpy_ca.py 128 16 128
converged with rank=16
Elapsed = 0.0032532215118408203

real	0m0,195s
user	0m0,174s
sys	0m0,021s
python3 numpy_ca.py 128 64 128
converged with rank=64
Elapsed = 0.009768009185791016

real	0m0,175s
user	0m0,161s
sys	0m0,012s
python3 numpy_ca.py 128 128 128
Elapsed = 0.019016027450561523

real	0m0,189s
user	0m0,172s
sys	0m0,016s
python3 numpy_ca.py 256 16 256
converged with rank=16
Elapsed = 0.00847768783569336

real	0m0,180s
user	0m0,145s
sys	0m0,032s
python3 numpy_ca.py 256 64 256
converged with rank=64
Elapsed = 0.026617765426635742

real	0m0,193s
user	0m0,185s
sys	0m0,008s
python3 numpy_ca.py 256 128 256
converged with rank=128
Elapsed = 0.051917076110839844

real	0m0,223s
user	0m0,197s
sys	0m0,024s
python3 numpy_ca.py 512 16 512
converged with rank=16
Elapsed = 0.038767337799072266

real	0m0,213s
user	0m0,196s
sys	0m0,016s
python3 numpy_ca.py 512 64 512
converged with rank=64
Elapsed = 0.13534021377563477

real	0m0,316s
user	0m0,300s
sys	0m0,016s
python3 numpy_ca.py 512 128 512
converged with rank=128
Elapsed = 0.2599155902862549

real	0m0,454s
user	0m0,426s
sys	0m0,028s
python3 numpy_ca.py 1024 16 1024
converged with rank=16
Elapsed = 0.1749591827392578

real	0m0,354s
user	0m0,324s
sys	0m0,024s
python3 numpy_ca.py 1024 64 1024
converged with rank=64
Elapsed = 0.7480988502502441

real	0m0,975s
user	0m0,940s
sys	0m0,032s
python3 numpy_ca.py 1024 128 1024
converged with rank=128
Elapsed = 1.304311990737915

real	0m1,631s
user	0m1,593s
sys	0m0,036s
python3 numpy_ca.py 2048 16 2048
converged with rank=16
Elapsed = 0.864260196685791

real	0m1,125s
user	0m0,941s
sys	0m0,184s
python3 numpy_ca.py 2048 64 2048
converged with rank=64
Elapsed = 3.3773536682128906

real	0m3,820s
user	0m3,212s
sys	0m0,608s
python3 numpy_ca.py 2048 128 2048
converged with rank=128
Elapsed = 6.494686603546143

real	0m7,211s
user	0m5,949s
sys	0m1,260s
done
./fortran_ca.out 128 16 128
 aprox rank is           16

real	0m0,005s
user	0m0,005s
sys	0m0,000s
./fortran_ca.out 128 64 128
 aprox rank is           64

real	0m0,015s
user	0m0,015s
sys	0m0,000s
./fortran_ca.out 128 128 128
 aprox rank is          128

real	0m0,026s
user	0m0,026s
sys	0m0,000s
./fortran_ca.out 256 16 256
 aprox rank is           16

real	0m0,019s
user	0m0,014s
sys	0m0,005s
./fortran_ca.out 256 64 256
 aprox rank is           64

real	0m0,070s
user	0m0,047s
sys	0m0,024s
./fortran_ca.out 256 128 256
 aprox rank is          128

real	0m0,145s
user	0m0,129s
sys	0m0,016s
./fortran_ca.out 512 16 512
 aprox rank is           16

real	0m0,083s
user	0m0,069s
sys	0m0,012s
./fortran_ca.out 512 64 512
 aprox rank is           64

real	0m0,311s
user	0m0,255s
sys	0m0,056s
./fortran_ca.out 512 128 512
 aprox rank is          128

real	0m0,624s
user	0m0,520s
sys	0m0,092s
./fortran_ca.out 1024 16 1024
 aprox rank is           16

real	0m0,350s
user	0m0,290s
sys	0m0,061s
./fortran_ca.out 1024 64 1024
 aprox rank is           64

real	0m1,280s
user	0m1,060s
sys	0m0,220s
./fortran_ca.out 1024 128 1024
 aprox rank is          128

real	0m2,602s
user	0m2,188s
sys	0m0,364s
./fortran_ca.out 2048 16 2048
 aprox rank is           16

real	0m1,613s
user	0m1,410s
sys	0m0,218s
./fortran_ca.out 2048 64 2048
 aprox rank is           64

real	0m6,216s
user	0m5,527s
sys	0m0,793s
./fortran_ca.out 2048 128 2048
 aprox rank is          128

real	0m12,578s
user	0m10,832s
sys	0m1,576s
done
