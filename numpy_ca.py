import numpy as np
import argparse

MAT_RANGE = 10
np.set_printoptions(precision=3, suppress=True)
def mat_gen(n: int, rank: int):
    mat_l = np.random.uniform(low=-MAT_RANGE, high=MAT_RANGE, size=(n, rank))
    mat_r = np.random.uniform(low=-MAT_RANGE, high=MAT_RANGE, size=(rank, n))
    return mat_l @ mat_r

def argmax_i(A, i_indicies, j_indicies, U, V, r, n):
    j_one = np.argmax(j_indicies > -1)
    val_max = 0
    argmax = 0
    for i in i_indicies:
        if i == -1:
            continue
        el = A(i, j_one)
        if U.size:
            el -= np.inner(U[i, :], V[:, j_one])
        if abs(val_max) < abs(el):
            val_max = el
            argmax = i
    return argmax

def argmax_j(A, i, j_indicies, U, V, r, n): # find first present j_index
    val_max = 0
    argmax = 0
    for j in j_indicies:
        if j == -1:
            continue
        el = A(i, j)
        if U.size:
            el -= np.inner(U[i, :], V[:, j])
        if abs(val_max) < abs(el):
            val_max = el
            argmax = j 
    return argmax, val_max

def calc_norm(old_norm, U, V, u, v):
    if U.size == 0:
        return 0
    return old_norm + 2 * np.inner((U.T @ u)[0], (V @ v.T)[0]) + (np.linalg.norm(u) * np.linalg.norm(v)) ** 2

def run_cross(A_func, n, eps):
    U = np.array([])
    V = np.array([])
    i_indicies = np.array(list(range(n)))
    j_indicies = np.array(list(range(n)))
    uv_norm = 0
    u = 0 # dummy
    v = 0
    r = 0
    while r < n:
        i = argmax_i(A_func, i_indicies, j_indicies, U, V, r, n)
        j, b_ij = argmax_j(A_func, i, j_indicies, U, V, r, n)
        uv_norm = calc_norm(uv_norm, U, V, u, v) 
        if eps * uv_norm >= abs(b_ij) * (n - r):
            break
        u = np.empty((n, 1))
        for t in range(n):
            u[t, 0] = A_func(t, j)
        if U.size:
            uv_j = U @ V[:, j]
            u -= uv_j[:, np.newaxis] 
        u /= np.sqrt(abs(b_ij))

        v = np.empty((1, n))
        for t in range(n):
            v[0][t] = A_func(i, t)
        if U.size:
            v -= U[i, :] @ V
        v = v * np.sqrt(abs(b_ij)) / b_ij

        U = np.hstack([U, u]) if U.size else u
        V = np.vstack([V, v]) if V.size else v
        i_indicies[i] = -1
        j_indicies[j] = -1
        r += 1
    return U, V, r


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int)
    parser.add_argument("rank", type=int)
    args = parser.parse_args()

    #A = mat_gen(args.n, args.rank)
    #A_func = lambda i, j: A[i][j]

    A_func = lambda i, j: (i + j + 1)
    U, V, r = run_cross(A_func, args.n, 1e-14)
    print(r)

